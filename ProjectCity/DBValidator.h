#pragma once
#include "Validator.h"
#include <regex>
class DBValidator :
	public Validator
{
public:
	DBValidator();
	~DBValidator();

	bool validate(string _value);
};

