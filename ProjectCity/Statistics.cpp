#include "stdafx.h"
#include "Statistics.h"


Statistics::Statistics()
{
	dMedian = 0;
	dAvg = 0;
	dSD = 0;
}


Statistics::~Statistics()
{
}

double Statistics::AVG(const vector<int>& _vTab)
{
	if (_vTab.size() != 0) {
		for (int i = 0; i < _vTab.size(); i++)
			iSum += _vTab[i];
		dAvg = (double)iSum / (double)_vTab.size();
	}
	else
		dAvg = 0;
	return dAvg;
}

double Statistics::median(const vector<int>& _vTab)
{
	double dCenter;
	if (_vTab.size() % 2 == 0) {
		dCenter = _vTab.size() / 2;
		dMedian = ((double)_vTab[(int)dCenter] + (double)_vTab[(int)dCenter]) / 2;
	}
	else {
		dCenter = ceil(_vTab.size() / 2);
		dMedian = _vTab[(int)dCenter];
	}
	return dMedian;
}

double Statistics::SD(const vector<int>& _vTab, double _dAvg)
{
	double dCSum = 0;
	for(int i = 0; i < _vTab.size(); i++) {
		dCSum += abs((double)_vTab[i] - _dAvg);
	}
	dSD = sqrt((double)dCSum / _vTab.size());
	return dSD;
}

int Statistics::sum(const vector<int>& _vTab)
{
	if (_vTab.size() != 0) {
		for (int i = 0; i < _vTab.size(); i++)
			iSum += _vTab[i];
	}
	else
		iSum = 0;
	return iSum;
}
