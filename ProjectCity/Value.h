#pragma once
#include "Validator.h"
#include <regex>

class Value :
	public Validator
{
public:
	Value();
	~Value();

	bool validate(string _value);
};

