#pragma once
#include "FileSupport.h"
#include "Player.h"
#include "Validator.h"
#include "Value.h"
#include <string>
#include <ctime>
#include <vector>
#include <algorithm>

class Game
{
private:
	int iStages;
	string sNickName;
	string sDBName;

	Validator *validator;
	Value valValidator;

	vector<int> vIncorrectRows;
	vector<int> vDrawnCityPairs;

	//private functions/methods
	string RandomCityPair(int _iStage);
	int NewQuestion(int _iStage);

	//display methods
	int SetQuestion(string _sCity, string _sCity1);
	void DispDistance(int _iCorrect, int _iAnswer, int _iStage);
	void DispWelcome();
public:
	Game(string _sDBName);
	~Game();

	void NewGame();
};

