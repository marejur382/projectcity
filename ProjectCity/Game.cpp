#include "stdafx.h"
#include "Game.h"

using namespace std;

Game::Game(string _sDBName)
{
	sDBName = _sDBName;
}


Game::~Game()
{
}

void Game::NewGame()
{
	int exit;
	do {
		DispWelcome();
		Player newPlayer(sNickName); //create new player
		for (int i = 0; i < iStages; i++)
			newPlayer.AddPoints(NewQuestion(i));
		newPlayer.ShowStats();
		cout << "Press ESC to exit or any other key to new game...";
		exit = _getch();
		system("cls");
	} while (exit != 27);
}

/**
New guestion function

@return points from answer
*/
int Game::NewQuestion(int _iStage)
{
	string sRandomCityPair = RandomCityPair(_iStage); //get database string
	string sCity, sCity1, sDistance; //each string variable from sRandomCityPair string
	size_t iPos, iPos1;	//position of ';' in databse string
	int iDistance, iAnswer;
	if (sRandomCityPair != "") {
		iPos = sRandomCityPair.find(';');
		iPos1 = sRandomCityPair.find(';', iPos + 1);
		sCity = sRandomCityPair.substr(0, iPos);
		sCity1 = sRandomCityPair.substr(iPos + 1, iPos1 - (iPos + 1));
		sDistance = sRandomCityPair.substr(iPos1 + 1);
		iDistance = stoi(sDistance);
	}
	else {
		cerr << "#ERROR: Critical error." << endl;
		cerr << "Press any key to exit...";
		_getch();
		exit(1);
	}

	cout << "-----STEGE NUMBER: " << _iStage + 1 << "-----" << endl;
	iAnswer = SetQuestion(sCity, sCity1);
	DispDistance(iDistance, iAnswer, _iStage);
	return abs(iDistance - iAnswer);
}

/**
get new random city pair from database

@return string line from database
*/
string Game::RandomCityPair(int _iStage)
{
	FileSupport newDatabase(sDBName);
	string sCityPair;
	srand(time(NULL));
	vIncorrectRows = newDatabase.vTab;
	int iRandomCityPair;
	bool bIsInIncorrect, bIsInDrawn;
	do {
		iRandomCityPair = ((rand() % newDatabase.DBSize()) + 1);
		bIsInIncorrect = find(vIncorrectRows.begin(), vIncorrectRows.end(), iRandomCityPair) != vIncorrectRows.end();
		if (_iStage < newDatabase.DBSize())
			bIsInDrawn = find(vDrawnCityPairs.begin(), vDrawnCityPairs.end(), iRandomCityPair) != vDrawnCityPairs.end();
		else {
			vDrawnCityPairs.clear();
			bIsInDrawn = false;
		}
	} while (bIsInIncorrect || bIsInDrawn);
	vDrawnCityPairs.push_back(iRandomCityPair);
	sCityPair = newDatabase.TakeCity(iRandomCityPair);
	return sCityPair;
}

//Display methods

/**
Set a new question

@param _sCity first city name
@param _sCity1 second city name
@return answer
*/
int Game::SetQuestion(string _sCity, string _sCity1)
{
	string sAnswer;
	cout << "How long distance is from " << _sCity << " to " << _sCity1 << "?\n";
	do {
		cout << "Answer: ";
		cin >> sAnswer;
		if (!validator->validate(sAnswer))
			cout << "Type a integer number..." << endl;
	} while (!validator->validate(sAnswer));
	system("cls");
	return stoi(sAnswer);
}

/**
Display distance and points

@param _sCity first city name
@param _sCity1 second city name
@return answer
*/
void Game::DispDistance(int _iCorrect, int _iAnswer, int _iStage)
{
	int iPoints = abs(_iCorrect - _iAnswer);
	cout << "Correct distance is: " << _iCorrect << endl;
	cout << "Your answer is: " << _iAnswer << endl;
	if (iPoints == 0)
		cout << "CONGRATULATIONS YOU TYPE A CORRECT DISTANCE!!" << endl;
	else if (iPoints < 100)
		cout << "Not bad. You got " << iPoints << " points." << endl;
	else 
		cout << "Nice try. You got " << iPoints << " points. Next time will be better!" << endl;
	
	if (_iStage + 1 != iStages)
		cout << "Press any key to take a next question...";
	else
		cout << "Press any key to exit...";

	_getch();
	system("cls");
}

/*
Display welcome text and take number of stages and nickname
*/
void Game::DispWelcome()
{
	string sStages;
	cout << "Welcome to the game 'Cities'" << endl;
	cout << "What is your name?: ";
	cin >> sNickName;
	validator = &valValidator;
	do {
		cout << "How many stages do you want to play?: ";
		cin >> sStages;
		if (!validator->validate(sStages))
			cout << "Type a integer number..." << endl;
	} while (!validator->validate(sStages));
	iStages = stoi(sStages);
	system("cls");
}
