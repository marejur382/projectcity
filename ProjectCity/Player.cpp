#include "stdafx.h"
#include "Player.h"


Player::Player(string _sNick)
{
	sNick = _sNick;
}


Player::~Player()
{
}

/*
Add points to player

@param _iPoint points to add
*/
void Player::AddPoints(int _iPoint)
{
	vResultsTab.push_back(_iPoint);
}

/**
Display statistics for player
*/
void Player::ShowStats()
{
	dAvg = sStats.AVG(vResultsTab);
	dMedian = sStats.median(vResultsTab);
	dSD = sStats.SD(vResultsTab, dAvg);
	iSum = sStats.sum(vResultsTab);

	cout << "Statistics for player: " << sNick << endl;
	cout << "Avarage: " << dAvg << endl;
	cout << "Median: " << dMedian << endl;
	cout << "Standard deviation: " << dSD << endl;
	cout << "Sum of points: " << iSum << endl;
	DispPoints();
}

/**
Display points table for player
*/
void Player::DispPoints()
{
	cout << "Your points: ";
	if (vResultsTab.size() != 0) {
		for (int i = 0; i < vResultsTab.size(); i++)
			cout << vResultsTab[i] << " ";
		cout << " ";
	}
	cout << endl;
}