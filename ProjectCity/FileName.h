#pragma once
#include "Validator.h"
#include <string>
#include <regex>

using namespace std;

class FileName:
	public Validator
{
public:
	FileName();
	~FileName();

	bool validate(string _sValue);
};

