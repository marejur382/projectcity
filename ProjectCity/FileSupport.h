#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include "Validator.h"
#include "DBValidator.h"
#include "FileName.h"

using namespace std;

class FileSupport
{
private:
	fstream file;	// file stream
	string sFileName;	// file name
	int iCityQuantity;	//number of city pair in database
	int iIncorrectRowsSize;
	Validator *validator;

	void DisplayTab();
	int CityQuantity();

public:
	vector<int> vTab;

	FileSupport();
	FileSupport(string _sFileName);
	~FileSupport();

	string TakeCity(int _iPos);
	int DBSize();
	void ValidateDatabase();
	void DisplayIncorrectRows();
	bool TryOpenDb(string _DBName);
};

