#pragma once
#pragma once
#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <cstdio>

using namespace std;

template<class T>
class DynamicTable
{
private:
	T *tab;
	T *buff;
	int iSize;
	bool bFirstInsert;
	double dSum;
	double dAVG;

public:
	DynamicTable(T _tab[], int _iSize);
	~DynamicTable();
	void InsertItem(T _item, int _iPlace);	//insert item into _iPlace
	void InsertItem(T _item); //insert item to end of table
	void DeleteItem();	//delete last item
	void DeleteItem(int _iPlace); //delete item from place
	void Display();	//display items in table
	void Sort();	//sorting table
	int ShowSize();	//show table size
	double AVG();
};

/**
@param _tab[] pointer to the table.
@param _iSize size of table.
*/
template <class T>
DynamicTable<T>::DynamicTable(T _tab[], int _iSize)
{
	this->tab = _tab;
	this->iSize = _iSize;
	if (_iSize == 0)
		this->bFirstInsert = true;
}

template <class T>
DynamicTable<T>::~DynamicTable()
{
	delete[] this->tab;
}

/**
Inserting new element to table

@param _tab[] pointer to table.
@param _iPlace place in which to insert new element.
*/
template <class T>
void DynamicTable<T>::InsertItem(T _item, int _iPlace)
{
	buff = new T[this->iSize + 1];
	realloc(this->tab, iSize + 1);
	for (int i = 0; i < _iPlace; ++i)
		buff[i] = this->tab[i];
	buff[_iPlace] = _item;
	for (int i = _iPlace + 1; i < this->iSize + 1; ++i)
		buff[i] = this->tab[i - 1];
	this->tab = buff;
	++this->iSize;
}
/**
Inserting new element to table to end

@param _tab[] pointer to table.
*/
template <class T>
void DynamicTable<T>::InsertItem(T _item)
{
	buff = new T[this->iSize + 1];
	if (this->bFirstInsert)
		this->bFirstInsert = false;
	else
		++this->iSize;
	memcpy(buff, this->tab, this->iSize * sizeof(T));
	buff[this->iSize] = _item;
	this->tab = buff;

}

/**
Deleteing last element in the table
*/
template <class T>
void DynamicTable<T>::DeleteItem()
{
	this->iSize -= 1;
	T *newTable = new T[this->iSize];
	memcpy(newTable, this->tab, sizeof(T) * this->iSize);
	//delete[] this->tab;
	this->tab = newTable;
	realloc(this->tab, iSize);
}

/**
Deleting element in a speceified place

@param _iPlace place in which to delete element.
*/
template <class T>
void DynamicTable<T>::DeleteItem(int _iPlace)
{
	iSize -= 1;
	T *newTable = new T[iSize];
	memcpy(newTable, this->tab, sizeof(T) * _iPlace - 1);
	memcpy(&newTable[_iPlace - 1], &this->tab[_iPlace], sizeof(T) * (iSize + 1 - _iPlace));
	this->tab = newTable;
}

/**
Displaying elements in table
*/
template <class T>
void DynamicTable<T>::Display()
{
	for (int i = 0; i < this->iSize; i++)
	{
		cout << this->tab[i];
		cout << " ";
	}
}

/**
Sorting elements in table
*/
template <class T>
void DynamicTable<T>::Sort()
{
	for (int i = 0; i < this->iSize - 1; i++)
		for (int j = 0; j < this->iSize - 1; j++)
			if (this->tab[j] > this->tab[j + 1])
			{
				T tmp = this->tab[j];
				this->tab[j] = this->tab[j + 1];
				this->tab[j + 1] = tmp;
			}
}

/**
Printing size of table

@return return size of table
*/
template <class T>
int DynamicTable<T>::ShowSize()
{
	return this->iSize;
}

/**
@return return avarage
*/
template <class T>
double DynamicTable<T>::AVG()
{
	if (this->iSize != 0) {
		for (int i = 0; i < this->iSize; i++)
			this->dSum += this->tab[i];
		this->dAVG = double(this->dSum) / double(this->iSize);
	}
	else dAVG = 0;

	return this->dAVG;
}