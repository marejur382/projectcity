#include "stdafx.h"
#include "FileName.h"


FileName::FileName()
{
}


FileName::~FileName()
{
}

bool FileName::validate(string _sValue)
{
	size_t iPos = _sValue.length() - 4;
	string sExt = _sValue.substr(iPos, 4);
	if (sExt == ".txt") {
		return 1;
	}
	return 0;
}
