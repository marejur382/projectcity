#pragma once
#include <string>
#include <vector>
#include "Statistics.h"

using namespace std;

class Player
{
private:
	int iGame; //how many questions
	string sNick;
	int iPoints;
	double dAvg;
	double dMedian;
	double dSD;
	int iSum;
	vector<int> vResultsTab;
	Statistics sStats;

	//display methods
	void DispPoints();

protected:


public:
	Player(string _sNick);
	~Player();

	void ShowStats();
	void AddPoints(int _iPoint);
};

