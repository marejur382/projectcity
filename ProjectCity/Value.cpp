#include "stdafx.h"
#include "Value.h"


Value::Value()
{
}


Value::~Value()
{

}

/**
Validate is _value integer

@param _value string value to validate
@return true - correct, false - incorrect
*/
bool Value::validate(string _value)
{
	regex regex_pattern("-?[0-9]+");

	return regex_match(_value, regex_pattern);
}