// ProjectCity.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Game.h"
#include "FileName.h"
#include "FileSupport.h"
#include "Validator.h"
#include <conio.h>
#include <stdio.h>


int main(int argc, char* argv[])
{
	FileSupport newDB;
	bool bTryOpenDb = newDB.TryOpenDb(argv[1]);
	newDB.~FileSupport();
	if (argc > 1) {
		if (bTryOpenDb) {
			Game newGame(argv[1]);
			newGame.NewGame();
			exit(1);
		}
	}
	else
		cout << "ERROR: Filed to load database name." << endl;

    return 0;
}
