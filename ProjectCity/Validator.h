#pragma once
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>

using namespace std;

class Validator
{
public:
	Validator();
	~Validator();

	virtual bool validate(string _value) = 0;
};

