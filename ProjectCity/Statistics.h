#pragma once
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

class Statistics
{
private:
	double dMedian;
	double dAvg;
	double dSD;
	int iSum;
public:
	Statistics();
	~Statistics();

	double AVG(const vector<int>& _vTab);
	double median(const vector<int>& _vTab);
	double SD(const vector<int>& _vTab, double _dAvg);
	int sum(const vector<int>& _vTab);
};

