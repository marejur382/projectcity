#include "stdafx.h"
#include "FileSupport.h"


FileSupport::FileSupport()
{
}

FileSupport::FileSupport(string _sFileName)
{
	sFileName = _sFileName;
	file.exceptions(ifstream::badbit | ifstream::failbit);
	CityQuantity();
	ValidateDatabase();
}


FileSupport::~FileSupport()
{
}

/**
Take city pair from database file

@param _iPos pair position in database file
@return line of city pair in database file
*/
string FileSupport::TakeCity(int _iPos)
{
	string buff = "";
	if (_iPos > iCityQuantity)
	{
		cerr << "#ERROR: That position is out off database file.";
		return "";
	}
	try {
		int dbIndex = 0;
		file.open(sFileName, ios::in);
		while (dbIndex != _iPos) {
			getline(file, buff);
			dbIndex++;
		}
		file.close();
	}
	catch (ifstream::failure e) {
		cerr << "#ERROR: Exception opening/reading/closing database file\n";
		cerr << "Press any key to exit...";
		_getch();
		exit(1);
	}
	return buff;
}
/**
@return cities pairs quantity in database file
*/
int FileSupport::CityQuantity()
{
	iCityQuantity = 0;
	string s = sFileName;
	try {
		file.open(sFileName, ios::in);
		string buff = "";
		if (file.is_open()) {
			while (!file.eof()) {
				try {
					getline(file, buff);
					iCityQuantity++;
				}
				catch (ifstream::failure e) {
					cerr << "#ERROR: Wrong end of database file.\n";
					cerr << "Press any key to exit...";
					_getch();
					exit(1);
				}
			}
		}
		file.close();
	}
	catch (ifstream::failure e) {
		cerr << "#ERROR: Exception opening/reading/closing database file\n";
		cerr << "Press any key to exit...";
		_getch();
		exit(1);
	}

	return iCityQuantity;
}

int FileSupport::DBSize()
{
	return iCityQuantity;
}

/**
Display information about incorrect records in database
*/
void FileSupport::DisplayIncorrectRows()
{
	CityQuantity();
	ValidateDatabase();
	if (iIncorrectRowsSize != 0) {
		cout << "#WARNING: Wrong database record at: \n#WARNING: ";
		DisplayTab();
		cout << "[Sum of wrong records: " << vTab.size() << "]\n";
		cout << "Press any key to continue...\n";
		_getch();
		system("cls");
	}
}

/**
Validate database file
*/
void FileSupport::ValidateDatabase()
{
	DBValidator dbValidator;
	validator = &dbValidator;
	int iSeek = 0;

	try {
		file.open(sFileName, ios::in);
		string buff = "";
		if (file.is_open()) {
			while (!file.eof()) {
				try {
					getline(file, buff);
					iSeek++;
				}
				catch (ifstream::failure e) {
					cerr << "#ERROR: Wrong end of database file.\n";
					cerr << "Press any key to exit...";
					_getch();
					exit(1);
				}
				if (!validator->validate(buff)) {
					vTab.push_back(iSeek);
				}
			}	
		}
		iIncorrectRowsSize = vTab.size();
		file.close();
	}
	catch (ifstream::failure e) {
		cerr << "#ERROR: Exception opening/reading/closing database file\n";
		cerr << "Press any key to exit...";
		_getch();
		exit(1);
	}
}

void FileSupport::DisplayTab()
{
	if (vTab.size() != 0) {
		for (int i = 0; i < vTab.size(); i++)
			cout << vTab[i] << " ";
		cout << " ";
	}
}

bool FileSupport::TryOpenDb(string _DBName)
{
	sFileName = _DBName;
	FileName fnValidator;
	validator = &fnValidator;
	fstream tFile;
	tFile.exceptions(ifstream::badbit | ifstream::failbit);
	if (validator->validate(sFileName)) {
		try {
			tFile.open(sFileName, ios::in);
			tFile.close();
			DisplayIncorrectRows();
			return 1;
		}
		catch (ifstream::failure e) {
			cerr << "#ERROR: Exception opening/reading/closing database file.\n#ERROR: Check if file exists." << endl;
			cerr << "Press any key to exit...";
			_getch();
			exit(1);
		}
	}
	else {
		cout << "ERROR: Wrogn file name. Remember that the database file must have extension '.txt'." << endl;
		cerr << "Press any key to exit...";
		_getch();
		exit(1);
	}
	return 0;
}