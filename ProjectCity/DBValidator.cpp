#include "stdafx.h"
#include "DBValidator.h"


DBValidator::DBValidator()
{
}


DBValidator::~DBValidator()
{
}
/**
Validate is _value 

@param _value string value to validate
@return true - correct, false - incorrect
*/
bool DBValidator::validate(string _value)
{
	regex regex_pattern("^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*;[a-zA-Z]+(?:[\s-][a-zA-Z]+)*;-?[0-9]+");

	return regex_match(_value, regex_pattern);
}
